import express from "express";
import mongoose from "mongoose";
import cookieParser from "cookie-parser";
import dotenv from "dotenv";
import cors from "cors";
import { urlencoded, json } from "body-parser";

const app = express();

app.use(
  urlencoded({ extended: false }),
  json(),
  cookieParser(),
  cors(),
);

const result = dotenv.config();

if (result.error) {
  throw result.error;
}

const port = 8080;

app.get("/test", (req, res) => {
  return res.status(200).send({ message: "test" });
});

app.all("*", (req, res) => {
  return res.status(200).send({ message: "Заблудился" });
});

mongoose
  .connect(process.env.DB_KEY || "", {
    useFindAndModify: false,
    autoIndex: false,
    poolSize: 10,
    bufferMaxEntries: 0,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Законектился к бд");
  })
  .catch((err) => {
    console.log("Что-то не так:", err);
  });

mongoose.set("debug", true);

app.listen(port, (err) => {
  if (err) throw err;
  console.log(`> Ready on localhost:${port}`);
});