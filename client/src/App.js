import React, { useState, useEffect } from 'react';
import axios from 'axios';

function App() {

  const [test, setTest] = useState('');

  async function fetchData() {
    const res = await axios.get("http://localhost:8080/test")
    setTest(res.data)
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      {test}
    </div>
  );
}

export default App;
